import discord
from yahoo_fin import stock_info as si

"""
Website that tells you how to setup a bot and register 
is for the server
https://www.devdungeon.com/content/make-discord-bot-python
Did some ground work in the yaho_test script
and cleared up some issues. Need to make opening, and current
price a list then in the for loops we just append to it to 
make it callable later on to find percentage change

Client ID: 
776896029210968165

#permisions integer:
2048

TOKEN:
Nzc2ODk2MDI5MjEwOTY4MTY1.X67izw.V7w2W-s28UUZWLVN2WN2LiRZNoE

"""
TOKEN = "Nzc2ODk2MDI5MjEwOTY4MTY1.X67izw.V7w2W-s28UUZWLVN2WN2LiRZNoE"

client = discord.Client()

stocks = [
    "aapl",
    "amzn",
    "fcel",
    "ntdoy",
]  # Will be filled with the current stocks being held
price = []  # List to keep the current stock prices
opening = []  # List to keep opening price


@client.event
async def on_message(message):
    # Makes sure the bot doens't respond to itself
    if message.author == client.user:
        return
    # Gives the price and perctage of movement for daily stock
    # from the Sure Thing Hedge Fund
    if message.content.startswith("!hedgefund"):
        # This should print out the stocks that the fund
        # is currently holding.
        msg = f"These are the stocks that the Sure Thing Hedge Fund is currently holding:\n {stocks} "
        await message.channel.send((msg))
    if message.content.startswith("!currentprice"):
        # Current price of each stock
        # print("Current stock price by ticker: \n")
        await message.channel.send("Current stock price by ticker: \n")
        for i in range(len(stocks)):
            price.append(si.get_live_price(stocks[i]))
            stk_msg = f"{stocks[i]}:{price[i]}"
            await message.channel.send(stk_msg)

    if message.content.startswith("!percentage"):
        # (current/open)-1 Gives the current percentage up
        """
        print("Current Percentage Change of Each Stock: \n")
        for i in range(len(stocks)):
            prcnt = si.get_quote_table(stocks[i])
            opening = prcnt['Opening']
        for i in range(len(stocks)):
            change = ((si.get_live_price(stocks[i]))/(opening))-1
            print(f"{stocks[i]} : {change}")
        """
        # await message.channel.send("Percentage change since opening:\n")
        for i in range(len(stocks)):
            prcnt = si.get_quote_table(stocks[i])
            opening.append(prcnt["Open"])

        await message.channel.send("Percentage change since opening:\n")
        for i in range(len(stocks)):
            price.append(si.get_live_price(stocks[i]))
            # print(price, " ", opening)
            change = ((price[i]) / (opening[i])) - 1
            print(f"{stocks[i]} : {100*change:.3f}%")
            prc_msg = f"{stocks[i]} : {100*change:.3f}%"
            await message.channel.send(prc_msg)


client.run(TOKEN)
