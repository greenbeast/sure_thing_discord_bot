# Sure Thing Discord Bot

Basic Discord bot for the Sure Thing Discord that keeps track of live price, percentage change, options etc for the stocks in the Sure Thing Hedge Fund. **MESO to the moon, baybee!!!!**

### Setup
- Clone this repo. If on Windows you will need to download [Git for Windows](https://phoenixnap.com/kb/how-to-install-git-windows) and if on Linux/Unix you probably have Git already installed. Once you have Git installed just run `git clone https://gitlab.com/greenbeast/sure_thing_discord_bot.git` to download the repo. Make sure you are in a directory that is easy to remember.
- Install [Python 3](https://phoenixnap.com/kb/how-to-install-python-3-windows) and go to your terminal/powershell and go to the directory you downloaded the code to and run `pip install -r requirements.txt`. 
- After the repo is cloned follow the steps for getting the [token](https://www.devdungeon.com/content/make-discord-bot-python) and verification info then either feel free to send that to me and I can update the source code or insert it into the `TOKEN` variable towards the top of the `sure_thing_bot.py`.
- Once the token is updated the `stocks` list will need to be updated as it is currently just filled with filler stocks to make sure it is all working. Once we get the Sure Thing stocks we can just replace the list with their info.
- After the code has been updated going into your terminal or Powershell if you're on Windows and `cd` to the directory that the script is in and run `python sure_thing_bot.py` and it will start running in the background. Just minimize the terminal/Powershell and you should be fine.

### Calling the bot
- Once the bot is up and running you can call it in the chat by running a couple of commands, all of which can be changed to whatever is easiest to remember but right now they are:

1) "hedgefund!" which will return the list of stocks currently invested in.

2) "currentprice!" which will return the price at the current time (though it does seem to be a bit off so it might be the price from 10-15 minutes before).

and

3) "percentage!" which will show the percentage change for the day.

- I am open to new ideas on functionalities and changes for the names of the calling commands.