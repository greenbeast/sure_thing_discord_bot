from yahoo_fin import stock_info as si

stocks = ["aapl", "amzn", "fcel", "ntdoy"]
price = []
opening = []
# print("Current stock price by ticker: \n")
for i in range(len(stocks)):
    price.append(si.get_live_price(stocks[i]))
    #print(f"{stocks[i]}: {price[i]}")

#print("Current Percentage Change for each ticker:", *stocks, sep='\n')
for i in range(len(stocks)):
    val = si.get_quote_table(stocks[i])
    opening.append(val["Open"])
    # print(f"{stocks[i]} : {opening}")

for i in range(len(stocks)):
    # print(price, " ", opening)
    change = ((price[i]) / (opening[i])) - 1
    print(f"{stocks[i]} : {100*change:.3f}%")
# val = si.get_quote_table('aapl')
# print(val['Open'])
