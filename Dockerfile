FROM python:3.8
RUN pip install discord yahoo_fin
COPY sure_thing_bot.py /tmp/
CMD ["python", "/tmp/sure_thing_bot.py"]